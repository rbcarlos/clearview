﻿var initialData = [
    { FirstName: "Well-Travelled Kitten", sales: 352, price: 75.95 },
    { FirstName: "Speedy Coyote", sales: 89, price: 190.00 },
    { name: "Furious Lizard", sales: 152, price: 25.00 },
    { name: "Indifferent Monkey", sales: 1, price: 99.95 },
    { name: "Brooding Dragon", sales: 0, price: 6350 },
    { name: "Ingenious Tadpole", sales: 39450, price: 0.35 },
    { name: "Optimistic Snail", sales: 420, price: 1.50 }
];

function Record(data) {
    this.Created = ko.observable(data.Created);
    this.Customer = ko.observable(data.Customer);
    this.Forwarder = ko.observable(data.Forwarder);
    this.Details = ko.observable();
}

function RecordViewModel() {
    var self = this;
    self.Records = ko.observableArray([]);
    self.Created = ko.observable();
    self.Customer = ko.observable();
    self.Forwarder = ko.observable();

    

    self.addRecord = function () {
        self.Records.push(new Record({
            Created: self.Created(),
            Customer: self.Customer(),
            Forwarder: self.Forwarder()
        }));
        self.Created(""),
        self.Customer(""),
        self.Forwarder("")
    };


    self.getDetails = function (data, event) {
        var context = ko.contextFor(event.target);
        var i = context.$index();
        $.getJSON("Scripts/detail.json", function (data) {
             data = ko.toJS(data);
             self.Records()[i].Details(data[i].detail);
        })
    }

    self.SendRecord = function (data, event) {
        var context = ko.contextFor(event.target);
        var i = context.$index();
        var d = self.Records()[i];
        alert('I would send this data: ' + ko.toJSON(d));
           
    };

    $.getJSON("Scripts/data.json", function (data) {
        data = ko.toJS(data);
        $.map(data, function (item) {
            self.Records.push(new Record({
                Created: item.name,
                Customer: item.sales,
                Forwarder: item.price
            }))
        })
    })

}

$(document).ready(function () {
    ko.applyBindings(new RecordViewModel());
});
