﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test Clearview</title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/style.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <table class="table" >
            <thead>
                <tr>
                    <th>Property Name</th>
                    <th>Enter Value</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Created by:</td>
                    <td>
                        <input data-bind="value: Created" />
                    </td>
                </tr>
                <tr>
                    <td>Customer:</td>
                    <td>
                        <input data-bind="value: Customer" />
                    </td>
                </tr>
                <tr>
                    <td>Forwarder :</td>
                    <td>
                        <input data-bind="value: Forwarder" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="button" class="btn btn-default"data-bind="click: addRecord">Add Record</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="table-responsive">
            <h2>List of Records</h2>
            <table class="table table-hover" data-bind="visible: Records().length > 0">
                <thead>
                    <tr>
                        <th>Created by</th>
                        <th>Customer</th>
                        <th>Forwarder</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: Records">
                    <tr>
                        <td><span data-bind="text: Created" /></td>
                        <td>
                            <input data-bind="value: Customer" />
                        </td>
                        <td>
                            <input data-bind="value: Forwarder" />
                        </td>
                        <td>
                            <li class="action-wrapper">
                                <a class="outer-link"><span class="glyphicon glyphicon-pencil"></span></a>
                                <ul>
                                    <li class="inner-action"><a role="button" data-toggle="collapse" data-bind="attr: { href: '#' + $index() }, click: $root.getDetails" ><span class="glyphicon glyphicon-plus"></span></a></li>
                                    <li class="inner-action"><a role="button" data-bind="click: $root.SendRecord"><span class="glyphicon glyphicon-envelope"></span></a></li>
                                    <li class="inner-action"><a><span class="glyphicon glyphicon-save"></span></a></li>
                                    <li class="inner-action"><a href="javascript:window.print()"><span class="glyphicon glyphicon-print"></span></a></li>
                                </ul>
                            </li>
                        </td>
                    </tr>
                    <tr width="100%" class="panel-collapse collapse" role="tabpanel" data-bind="attr: { id: $index }">
                        <td width="100%" colspan="4" data-bind="text: Details"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 
    <script src="Scripts/jquery-3.1.1.js"></script>
    <script src="Scripts/jquery.tmpl.min.js"></script>
    <script src="Scripts/knockout-3.4.2.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/app.js"></script>
    <script src="Scripts/knockout-3.4.2.debug.js"></script>
</body>
</html>
